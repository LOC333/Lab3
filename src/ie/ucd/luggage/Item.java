package ie.ucd.luggage;

public interface Item {

	
	public String getType();
	public double getWeight();
	public boolean isDangerous();
	
}
