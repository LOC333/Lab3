package ie.ucd.luggage;

public class Bomb implements Item{
	
	private double weight;
	private String type;
	
	public Bomb(double weight, String type) {
		this.weight = weight;
		this.type = type;
	}
	public String getType() {
		return type;
	}
	public double getWeight() {
		return weight;
	}
	public boolean isDangerous() {
		return true;
	}
}
