package ie.ucd.luggage;

public class Main {
	
	public static void main(String[] args) {
		Suitcase mySuitcase = new Suitcase(2);
		mySuitcase.add(new DesignerPen(1, "Designer Pen", "Mont Blanc"));
		mySuitcase.add(new Bomb(5,"Bomb"));
		mySuitcase.add(new Laptop(3,"laptop"));
		System.out.print("\nBag weight is: " +mySuitcase.getWeight());
		System.out.print("\nBag is dangerous: "+mySuitcase.isDangerous());
		mySuitcase.removeItem(1);
		System.out.print("\nBag weight is: " +mySuitcase.getWeight());
		System.out.print("\nBag is dangerous: "+mySuitcase.isDangerous());
		
		safeLuggage mySafeBag = new safeLuggage(3,"password9876");
		String givePassword = "password9876";
		mySafeBag.add((new DesignerPen(2, "Designer Pen","Bic")), givePassword);
		mySafeBag.add((new Laptop(5, "laptop")), givePassword);
		mySafeBag.add((new Bomb(2,"bomb")),"I don't know");
	}
	
}
