package ie.ucd.luggage;

public class Laptop implements Item{
	
	private double weight;
	private String type;

	public Laptop(double weight, String type) {
		this.weight = weight;
		this.type = type;
	}
	public String getType() {
		return type;
	}
	public double getWeight() {
		return weight;
	}
	public boolean isDangerous() {
		return false;
	}

}
