package ie.ucd.luggage;

public class DesignerPen extends Pen{
	
	private String brand;
	
	public DesignerPen(double weight, String type, String brand) {
		super(weight, type);
		this.brand = brand;
	}
	
	public String getBrand() {
		return brand;
	}
}
