package ie.ucd.luggage;

public class safeLuggage extends Suitcase{
	
	
	private String password;
	
	public safeLuggage(double bagWeight, String password){
		super(bagWeight);
		this.password = password;
	}
	
	public void add(Item item, String givePassword) {
		if(givePassword == password) {
			super.add(item);
		}
		else{
		 System.out.print("\nIncorrect Password. Item not added to bag.");
		}
	}
	
	public void remove(int index, String givePassword) {
		if(givePassword == password) {
			super.removeItem(index);
		}
		else{
		 System.out.print("\nIncorrect Password. Item not removed from bag.");
		}
	}
	
}
